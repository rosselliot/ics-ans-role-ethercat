import os
import pytest
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-ethercat-no-device-el')


@pytest.mark.parametrize("name", ["ec_master", "ec_generic"])
def test_kernel_modules_available(host, name):
    cmd = host.run(f"/sbin/modinfo {name}")
    assert f"{name}.ko" in cmd.stdout
